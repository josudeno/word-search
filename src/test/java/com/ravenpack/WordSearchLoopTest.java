package com.ravenpack;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for class {@link WordSearchLoop}
 *
 * O(s×m)
 */
@RunWith(JUnitParamsRunner.class)
public class WordSearchLoopTest extends TestBase {

    /** The class under test. */
    private WordSearchLoop classUnderTest;

    /**
     * Test that we can write simple message with the letters given. {@link WordSearchLoop#canMessageBeWritten(String, String)}
     */
    @Test
    @Parameters({
            "5000, hello",
            "10000, hello",
            "500000, hello"
    })
    @TestCaseName("{0}")
    public void testWeCanWriteTheMessageWhenLettersAreSorted(final Integer amountOfLetters, final String messageToWrite) {
        // GIVEN a message to write
        // AND letters in a bowl.
        final String fullAlphabet = "abcdefghijklmnopqrstuvwxyz";
        final String lettersInBowl = generateRandomLetters(fullAlphabet, amountOfLetters);

        // WHEN calling the method canMessageBeWritten
        classUnderTest = new WordSearchLoop();
        boolean result = classUnderTest.canMessageBeWritten(messageToWrite, lettersInBowl);

        // THEN expect the word to be able to be written.
        assertTrue("Expected message to be able to be written", result);
    }

    /**
     * Test that we can write simple message with the letters given. {@link WordSearchLoop#canMessageBeWritten(String, String)}
     */
    @Test
    @Parameters({
            "5, hello, olhel",
            "7, hello, oellhpo",
            "10, hello, olhlieopos",
            "15, hello, oflhosptiholoey",
            "25, hello, oohyebrifotmlyfhhoptswldq",
            "50, hello, ooylhhoqfrmioohwyodwftihohmsptteersbyltbfoydfphllq",
            "50, hello, olwowopsetbmfirhfehqoftmblhldfptyhslooiyhdrohyytoq" })
    @TestCaseName("{0}")
    public void testWeCanWriteTheMessageWhenWeHaveAllTheLetters(final String scenario, final String messageToWrite, final String bowlOfLetters) {
        // GIVEN a message to write
        // AND letters in a bowl.

        // WHEN calling the method canMessageBeWritten
        classUnderTest = new WordSearchLoop();
        boolean result = classUnderTest.canMessageBeWritten(messageToWrite, bowlOfLetters);

        // THEN expect the word to be able to be written.
        assertTrue("Expected message to be able to be written", result);
    }

    /**
     * Test that we can write simple message with the letters given. {@link WordSearchLoop#canMessageBeWritten(String, String)}
     */
    @Test
    @Parameters({
            "50, repetition",
            "50, repetition",
            "500, repetition",
            "5000, repetition",
            "50000, repetition",
            "500000, repetition",
            "5000000, repetition",
            "50000000, repetition"
    })
    @TestCaseName("{0}")
    public void testWeCannotWriteTheMessageWithTheGivenLetters(final Integer amountOfLetters, final String messageToWrite) {
        // GIVEN a message to write
        // AND letters in a bowl.
        final String alphabetWithNoN = "abcdefghijklmopqrstuvwxyz";
        final String lettersInBowl = generateRandomLetters(alphabetWithNoN, amountOfLetters);

        // WHEN calling the method canMessageBeWritten
        classUnderTest = new WordSearchLoop();
        boolean result = classUnderTest.canMessageBeWritten(messageToWrite, lettersInBowl);

        // THEN expect the word to not be able to be written.
        assertFalse("Expected message to be able to be written", result);
    }


    /**
     * Test that we can not write the message because we only have one instance of x in our bowl of letters.
     */
    @Test
    public void testWeCannotWriteTheMessageWithFullAlphabet() {
        // GIVEN a message to write
        final String messageToWrite = "xfox";

        // AND letters in a bowl.
        final String lettersInBowl = "zfoqx";

        // WHEN calling the method canMessageBeWritten
        classUnderTest = new WordSearchLoop();
        boolean result = classUnderTest.canMessageBeWritten(messageToWrite, lettersInBowl);

        // THEN expect the word to not be able to be written because there is only one x in the bowl of letters.
        assertFalse("Expected message to not be able to be written", result);
    }

    /**
     * Test that we can write the message fox.
     */
    @Test
    public void testWeCanWriteFoxUsingFullAlphabet() {
        // GIVEN a message to write
        final String messageToWrite = "fox";

        // AND letters in a bowl.
        final String lettersInBowl = "thequickbrownfoxjumpsoverthelazydog";

        // WHEN calling the method canMessageBeWritten
        classUnderTest = new WordSearchLoop();
        boolean result = classUnderTest.canMessageBeWritten(messageToWrite, lettersInBowl);

        // THEN expect the word to not be able to be written because there is only one x in the bowl of letters.
        assertTrue("Expected message to not be able to be written", result);
    }
}
