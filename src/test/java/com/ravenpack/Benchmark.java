package com.ravenpack;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertTrue;

/**
 * Benchmark class.
 */
@RunWith(JUnitParamsRunner.class)
public class Benchmark extends TestBase {

    /** The logger instance. */
    private final Logger logger = new Logger();

    /**
     * Test that we can write simple message with the letters given. {@link WordSearchLoop#canMessageBeWritten(String, String)}
     */
    @Test
    @Parameters({
            "7, hello, oellhpo",
            "10, hello, olhlieopos",
            "15, hello, oflhosptiholoey",
            "25, hello, oohyebrifotmlyfhhoptswldq",
            "50, hello, ooylhhoqfrmioohwyodwftihohmsptteersbyltbfoydfphllq",
            "50, hello, olwowopsetbmfirhfehqoftmblhldfptyhslooiyhdrohyytoq" })
    @TestCaseName("{0}")
    public void testWordSearchImp(final String scenario, final String messageToWrite, final String bowlOfLetters) {
        // GIVEN a message to write
        // AND letters in a bowl.

        // WHEN calling the method canMessageBeWritten
        boolean result = logger.logTime(new WordSearchLoop(), messageToWrite, bowlOfLetters);

        // THEN expect the word to be able to be written.
        assertTrue("Expected message to be able to be written", result);
    }

    /**
     * Test that we can write simple message with the letters given. {@link WordSearchLoop#canMessageBeWritten(String, String)}
     */
    @Test
    @Parameters({
            "7, hello, oellhpo",
            "10, hello, olhlieopos",
            "15, hello, oflhosptiholoey",
            "25, hello, oohyebrifotmlyfhhoptswldq",
            "50, hello, ooylhhoqfrmioohwyodwftihohmsptteersbyltbfoydfphllq",
            "100, hello, olwowopsetbmfirhfehqoftmblhldfptyhslooiyhdrohyytoqolwowopsetbmfirhfehqoftmblhldfptyhslooiyhdrohyytoq",
            "200, hello, olwowopsetbmfirhfehqoftmblhldfptyhslooiyhdrohyytoqolwowopsetbmfirhfehqoftmblhldfptyhslooiyhdrohyytoqolwowopsetbmfirhfehqoftmblhldfptyhslooiyhdrohyytoqolwowopsetbmfirhfehqoftmblhldfptyhslooiyhdrohyytoq"
    })
    @TestCaseName("{0}")
    public void testWeCanWriteTheMessage(final String scenario, final String messageToWrite, final String bowlOfLetters) {
        // GIVEN a message to write
        // AND letters in a bowl.

        // WHEN calling the method canMessageBeWritten
        logger.logTime(new WordSearchLoop(), messageToWrite, bowlOfLetters);
    }

    /**
     * Test that we cannot write simple message with the letters given. {@link WordSearchLoop#canMessageBeWritten(String, String)}
     */
    @Test
    @Parameters({
            "10, repetition",
            "10, repetition",
            "100, repetition",
            "1000, repetition",
            "10000, repetition",
            "100000, repetition",
            "1000000, repetition",
            "10000000, repetition",
            "100000000, repetition"
    })
    @TestCaseName("{0}")
    public void testWeCannotWriteTheMessageWithTheGivenLetters(final Integer amountOfLetters, final String messageToWrite) {
        // GIVEN a message to write
        // AND letters in a bowl.
        final String alphabetWithNoN = "abcdefghijklmopqrstuvwxyz";
        final String lettersInBowl = generateRandomLetters(alphabetWithNoN, amountOfLetters);

        // WHEN calling the method canMessageBeWritten
        logger.logTime(new WordSearchLoop(), messageToWrite, lettersInBowl);
    }



}
