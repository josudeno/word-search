package com.ravenpack;

import java.util.Random;

public class TestBase {

    /**
     * Generates random letters.
     *
     * @param amount The amount of letters we expect in return.
     * @return The letters generated.
     */
    protected String generateRandomLetters(final String alphabet, final Integer amount) {
        final Random random = new Random();

        final char[] characters = new char[amount];
        for (int i = 0; i < amount; i++) {
            characters[i] = alphabet.charAt(random.nextInt(alphabet.length()));
        }
        return new String(characters);
    }
}
