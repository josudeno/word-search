package com.ravenpack;

/**
 * Class in charge of searching for messages given a list of letters.
 *
 * Complexity O(m*s)
 */
public class WordSearchLoop implements WordSearch {

    /**
     * Checks whether the message can be written with the letters provided.
     *
     * @param message The expected message to write.
     * @param bowlOfLetters The letters provided to build the message.
     * @return if the message can be written.
     */
    public boolean canMessageBeWritten(final String message, final String bowlOfLetters) {
        char[] letters = bowlOfLetters.toCharArray();
        char[] messageArray = message.toCharArray();

        int count = 0;

        for (char messageChar : messageArray) {
            int index = found(messageChar, letters);
            if (index == -1) {
                return false;
            }
            letters[index] = ' '; // change character so is not usable again
            count++;
            if (message.length() == count) {
                return true;
            }
        }

        return true;
    }


    /**
     * Returns the index of the char in the array of letters or -1 if could not be found.
     *
     * @param toFind The char to find.
     * @param letters The array of letters to look into.
     *
     * @return the index of the char in letters
     */
    private int found(final char toFind, final char[] letters) {
        for (int i = 0; i < letters.length; i++) {
            if (letters[i] == toFind) {
                return i;
            }
        }
        return -1;
    }
}
