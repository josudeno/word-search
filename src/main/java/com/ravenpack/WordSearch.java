package com.ravenpack;


public interface WordSearch {

    /**
     * Checks whether the message can be written with the letters provided.
     *
     * @param message The expected message to write.
     * @param bowlOfLetters The letters provided to build the message.
     * @return if the message can be written.
     */
    boolean canMessageBeWritten(final String message, final String bowlOfLetters);
}
