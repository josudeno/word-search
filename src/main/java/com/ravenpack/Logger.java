package com.ravenpack;

/**
 * Logging class to asses performance of the method.
 */
class Logger {

    /**
     * Logs the time and returns whether if was successful to write the message.
     *
     * @param wordSearch The implementation of WordSearch.
     * @param message    The message.
     * @param bowlOfLetters The letters in the bowl.
     *
     * @return returns the result of the function.
     */
    boolean logTime(final WordSearch wordSearch, final String message, final String bowlOfLetters) {
        long startTime = System.nanoTime();

        boolean res = wordSearch.canMessageBeWritten(message, bowlOfLetters);

        long endTime = System.nanoTime();

        System.out.println(
                String.format(
                        "Time taken for message: '%s' %dns having %d letters using imp %s",
                        message,
                        endTime - startTime,
                        bowlOfLetters.length(),
                        wordSearch.getClass().getSimpleName()
                )
        );

        return res;
    }
}
